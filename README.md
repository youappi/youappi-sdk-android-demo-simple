### What is this repository for? ###

This repository contains 3 Android demo apps showing how to use YouAppi's Android SDK:

* **AppDemo** - A demo app showing how to use YouAppi's SDK
* **AppDemoAdMob** - A demo app showing how to use YouAppi's SDK with AdMob adapter.
* **AppdDemoMoPub** - A demo app showing how to use YouApp'd SDK with MoPub adapter.

Each app includes 3 buttons for showing ads for the following each ad unit types:

* Rewarded Video
* Interstitial Video
* Interstitial Ad

### How do I get set up? ###

Load the project with Android Studio and Gradle will do the rest.

### How do I run? ###

* On the project run menu select the desired demo app you would like to run.
* Press the run button and wait for the demo app to load.
